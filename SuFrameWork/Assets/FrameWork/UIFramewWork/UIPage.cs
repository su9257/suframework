﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Custom.Log;
#endregion
using Vuforia;
public class UIPage : UIPanel
{
    [FindChildNode]
    public GameObject TestObj = null;
    [FindChildNode()]
    public GameObject TestObj1 = null;
    [FindChildNode(nodeName = "123")]
    public GameObject TestObj2 = null;
    [FindChildNode()]
    public GameObject TestObj3 = null;

    //public GameObject Daxie = null;
    //public GameObject daxie = null;


    /// <summary>
    /// 返回按钮，大部分Page都会有返回按钮
    /// </summary>
    [SerializeField]
    private Button CloseButton = null;

    /// <summary>
    /// 打开UI的参数
    /// </summary>
    protected object OpenArg;

    /// <summary>
    /// 该UI的当前实例是否曾经被打开过
    /// </summary>
    private bool IsOpenedOnce;


    protected sealed override void Awake()
    {
        base.Awake();
    }

    void Start()
    {
        this.Log("子物体：Start");
        Debuger.Log("Debuger.Log");
    }

    /// <summary>
    /// 当UIPage被激活时调用
    /// </summary>
    protected void OnEnable()
    {
        this.Log("OnEnable()");
        if (CloseButton != null)
        {
            CloseButton.onClick.AddListener(CloseButtonOnClick);
        }
#if UNITY_EDITOR
        if (IsOpenedOnce)
        {
            //如果UI曾经被打开过，
            //则可以通过UnityEditor来快速触发Open/Close操作
            //方便调试
            OnOpen(OpenArg);
        }
#endif
    }

    /// <summary>
    /// 当UI不可用时调用
    /// </summary>
    protected void OnDisable()
    {
        this.Log("OnDisable()");
#if UNITY_EDITOR
        if (IsOpenedOnce)
        {
            //如果UI曾经被打开过，
            //则可以通过UnityEditor来快速触发Open/Close操作
            //方便调试
            OnClose();
        }
#endif
        if (CloseButton != null)
        {
            CloseButton.onClick.RemoveAllListeners();
        }
    }


    /// <summary>
    /// 当点击“返回”时调用
    /// 但是并不是每一个Page都有返回按钮
    /// </summary>
    private void CloseButtonOnClick()
    {
        this.Log("CloseButtonOnClick()");
        //UIManager.Instance.GoBackPage();
    }

    /// <summary>
    /// 调用它打开UIPage
    /// </summary>
    /// <param name="arg"></param>
    public sealed override void Open(object arg = null)
    {
        this.Log("Open()");
        OpenArg = arg;
        IsOpenedOnce = false;

        if (!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }

        OnOpen(arg);
        IsOpenedOnce = true;
    }

    public sealed override void Close(object arg = null)
    {
        this.Log("Close()");
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }

        OnClose(arg);
    }


}
