﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
#endregion

using Custom.Log;
using System.Reflection;

public class UIPanel : MonoBehaviour
{
    protected virtual void Awake()
    {
        System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
        watch.Start();
        this.Log($"开始初始化UI特性 Start 线程ID： {Thread.CurrentThread.ManagedThreadId.ToString("00")} ");
        InitAttribute();
        watch.Stop();
        this.Log($"初始化UI特性完成   End 线程ID：{Thread.CurrentThread.ManagedThreadId.ToString("00")} 耗时： {watch.ElapsedMilliseconds} 毫秒");
    }

    /// <summary>
    /// UI框架对应特性操作
    /// </summary>
    private void InitAttribute()
    {
        Type type = this.GetType();
        Dictionary<int, NodeInfo> UINodeInfoDictionnary = new Dictionary<int, NodeInfo>();
        //获取所有节点信息
        foreach (var node in GetComponentsInChildren<Transform>())
        {
            NodeInfo tempNodeInfo;
            tempNodeInfo.nodeName = node.name;
            tempNodeInfo.Node = node.gameObject;
            UINodeInfoDictionnary.Add(node.GetInstanceID(), tempNodeInfo);
        }

        foreach (var item in type.GetFields())//检测字段是否含有指定的特性
        {
            this.Log(item.Name);
            if (item.IsDefined(typeof(FindChildNodeAttribute), true))//检测寻找FGameObject特性
            {
                object oAttribute = item.GetCustomAttributes(typeof(FindChildNodeAttribute), true)[0];

                FindChildNodeAttribute attribute = oAttribute as FindChildNodeAttribute;

                if (string.IsNullOrEmpty(attribute.nodeName))//判断是否指定寻找Node的名称
                {
                    attribute.nodeName = item.Name;
                }
                //寻找Node
                GameObject tempGameObject = attribute.FindChildNode(UINodeInfoDictionnary);

                item.SetValue(this, tempGameObject);
            }
        }

        UINodeInfoDictionnary.Clear();
    }


    #region 框架基础
    public virtual void Open(object arg = null)
    {
        this.Log("Open() arg:{0}", arg);
    }

    public virtual void Close(object arg = null)
    {
        this.Log("Close() arg:{0}", arg);
    }

    /// <summary>
    /// 当前UI是否打开
    /// </summary>
    public bool IsOpen { get { return this.gameObject.activeSelf; } }

    /// <summary>
    /// 当UI关闭时，会响应这个函数
    /// 该函数在重写时，需要支持可重复调用
    /// </summary>
    protected virtual void OnClose(object arg = null)
    {
        this.Log("OnClose()");
    }

    /// <summary>
    /// 当UI打开时，会响应这个函数
    /// </summary>
    /// <param name="arg"></param>
    protected virtual void OnOpen(object arg = null)
    {
        this.Log("OnOpen() ");
    }
    #endregion
}
