﻿using System;


namespace PureMVC.Interfaces
{
    public interface IController
    {

        /// <summary>
        /// 注册命令接口
        /// </summary>
        /// <param name="notificationName"></param>
        /// <param name="commandType"></param>
        void RegisterCommand(string notificationName, Type commandType);


		void ExecuteCommand(INotification notification);


		void RemoveCommand(string notificationName);


		bool HasCommand(string notificationName);
	}
}
