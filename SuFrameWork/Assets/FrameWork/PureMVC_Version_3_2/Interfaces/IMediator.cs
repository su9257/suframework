﻿/* 
 PureMVC C# Port by Andy Adamczak <andy.adamczak@puremvc.org>, et al.
 PureMVC - Copyright(c) 2006-08 Futurescale, Inc., Some rights reserved. 
 Your reuse is governed by the Creative Commons Attribution 3.0 License 
*/

#region Using

using System;
using System.Collections.Generic;

#endregion

namespace PureMVC.Interfaces
{
    public interface IMediator
	{
		/// <summary>
        /// Tthe <c>IMediator</c> instance name
        /// </summary>
		string MediatorName { get; }
		
        /// <summary>
        /// The <c>IMediator</c>'s view component
        /// </summary>
		object ViewComponent { get; set; }

		/// <summary>
        /// List <c>INotification interests</c>
        /// </summary>
        /// <returns>An <c>IList</c> of the <c>INotification</c> names this <c>IMediator</c> has an interest in</returns>
        IList<string> ListNotificationInterests();
		
        /// <summary>
        /// Handle an <c>INotification</c>
        /// </summary>
        /// <param name="notification">The <c>INotification</c> to be handled</param>
		void HandleNotification(INotification notification);
		
		/// <summary>
		/// Called by the View when the Mediator is registered
		/// </summary>
		void OnRegister();

		/// <summary>
		/// Called by the View when the Mediator is removed
		/// </summary>
		void OnRemove();
	}
}
