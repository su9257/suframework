﻿using System;


namespace PureMVC.Interfaces
{
    /// <summary>
    /// 命令接口
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// 执行命令
        /// </summary>
        /// <param name="notification"></param>
		void Execute(INotification notification);
    }
}
