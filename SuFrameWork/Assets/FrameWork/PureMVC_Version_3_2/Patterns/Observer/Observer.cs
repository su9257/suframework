﻿using System;
using System.Reflection;

using PureMVC.Interfaces;


namespace PureMVC.Patterns
{

    public class Observer : IObserver
    {
        /// <summary>
        /// 对应函数名称
        /// </summary>
        private string m_notifyMethod;
        /// <summary>
        /// 要执行handleNotification函数对应继承Mediator的类
        /// </summary>
        private object m_notifyContext;
        protected readonly object m_syncRoot = new object();
        public Observer(string notifyMethod, object notifyContext)
        {
            m_notifyMethod = notifyMethod;
            m_notifyContext = notifyContext;
        }
        public virtual string NotifyMethod
        {
            private get
            {
                return m_notifyMethod;
            }
            set
            {
                m_notifyMethod = value;
            }
        }

        public virtual object NotifyContext
        {
            private get
            {
                return m_notifyContext;
            }
            set
            {
                m_notifyContext = value;
            }
        }
        /// <summary>
        /// 执行消息 通过反射找到需要执行handleNotification所属的类
        /// </summary>
        /// <param name="notification"></param>
        public virtual void NotifyObserver(INotification notification)
        {
            object context;
            string method;

            lock (m_syncRoot)
            {
                context = NotifyContext;
                method = NotifyMethod;
            }

            Type t = context.GetType();
            BindingFlags f = BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase;
            MethodInfo mi = t.GetMethod(method, f);
            mi.Invoke(context, new object[] { notification });
        }
        public virtual bool CompareNotifyContext(object obj)
        {
            lock (m_syncRoot)
            {
                return NotifyContext.Equals(obj);
            }
        }

    }
}
