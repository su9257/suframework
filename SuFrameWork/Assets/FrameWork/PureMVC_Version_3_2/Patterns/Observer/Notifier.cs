﻿
using System;

using PureMVC.Interfaces;
using PureMVC.Patterns;
namespace PureMVC.Patterns
{
    /// <summary>
    /// 通知类 可以直接SendNotification发送消息
    /// </summary>
    public class Notifier : INotifier
    {
        private IFacade m_facade = PureMVC.Patterns.Facade.Instance;
        protected IFacade Facade
        {
            get { return m_facade; }
        }
        public virtual void SendNotification(string notificationName) 
		{
			m_facade.SendNotification(notificationName);
		}


		public virtual void SendNotification(string notificationName, object body)
		{
			m_facade.SendNotification(notificationName, body);
		}


		public virtual void SendNotification(string notificationName, object body, string type)
		{
            m_facade.SendNotification(notificationName, body, type);
		}


	}
}
