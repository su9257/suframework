﻿using System;
using System.Collections.Generic;

using PureMVC.Interfaces;
using PureMVC.Patterns;


namespace PureMVC.Patterns
{
    /// <summary>
    /// 视图层
    /// </summary>
    public class Mediator : Notifier, IMediator, INotifier
	{
		public const string NAME = "Mediator";
        protected string m_mediatorName;
        protected object m_viewComponent;
        public virtual string MediatorName
        {
            get { return m_mediatorName; }
        }
        /// <summary>
        /// 对应视图的MonoBehaviour类
        /// </summary>
        public virtual object ViewComponent
        {
            get { return m_viewComponent; }
            set { m_viewComponent = value; }
        }

        public Mediator()
            : this(NAME, null)
        {
		}
        public Mediator(string mediatorName)
            : this(mediatorName, null)
        {
		}

		public Mediator(string mediatorName, object viewComponent)
		{
			m_mediatorName = (mediatorName != null) ? mediatorName : NAME;
			m_viewComponent = viewComponent;
		}
        /// <summary>
        /// 关注消息列表
        /// </summary>
        /// <returns></returns>
		public virtual IList<string> ListNotificationInterests()
		{
			return new List<string>();
		}
        /// <summary>
        /// 处理所关注的消息
        /// </summary>
        /// <param name="notification"></param>
		public virtual void HandleNotification(INotification notification)
		{
		}
		public virtual void OnRegister()
		{
		}

		public virtual void OnRemove()
		{
		}
	}
}
