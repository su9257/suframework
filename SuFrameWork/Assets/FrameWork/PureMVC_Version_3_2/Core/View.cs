﻿
using System;
using System.Collections.Generic;

using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace PureMVC.Core
{
    /// <summary>
    /// 视图核心 保存所有注册的消息和对应的Mediator视图层
    /// </summary>
    public class View : IView
    {
        #region Single
        /// <summary>
        /// 所有注册的视图层
        /// </summary>
        protected IDictionary<string, IMediator> m_mediatorMap;
        /// <summary>
        /// 所有注册消息的对应的观察者类 一个消息可以对应多个执行类
        /// </summary>
        protected IDictionary<string, IList<IObserver>> m_observerMap;
        protected static volatile IView m_instance;
        protected readonly object m_syncRoot = new object();
        protected static readonly object m_staticSyncRoot = new object();
        static View()
        {
        }
        protected View()
        {
            m_mediatorMap = new Dictionary<string, IMediator>();
            m_observerMap = new Dictionary<string, IList<IObserver>>();
            InitializeView();
        }
        protected virtual void InitializeView()
        {
        }
        public static IView Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (m_staticSyncRoot)
                    {
                        if (m_instance == null) m_instance = new View();
                    }
                }

                return m_instance;
            }
        }
        #endregion

        /// <summary>
        /// 注册观察者
        /// </summary>
        /// <param name="notificationName">消息名称</param>
        /// <param name="observer">包含handleNotification函数名称和执行这个函数的对应的类</param>
        public virtual void RegisterObserver(string notificationName, IObserver observer)
        {
            lock (m_syncRoot)
            {
                if (!m_observerMap.ContainsKey(notificationName))
                {
                    m_observerMap[notificationName] = new List<IObserver>();
                }

                m_observerMap[notificationName].Add(observer);
            }
        }
        /// <summary>
        /// 通知对应消息的所有Observer
        /// </summary>
        /// <param name="notification">包含消息的名称 所带的参数 设定的类型</param>
        public virtual void NotifyObservers(INotification notification)
        {
            IList<IObserver> observers = null;

            lock (m_syncRoot)
            {
                if (m_observerMap.ContainsKey(notification.Name))//获取消息名称
                {
                    IList<IObserver> observers_ref = m_observerMap[notification.Name];
                    observers = new List<IObserver>(observers_ref);
                }
            }
            if (observers != null)
            {
                for (int i = 0; i < observers.Count; i++)
                {
                    IObserver observer = observers[i];
                    observer.NotifyObserver(notification);
                }
            }
        }
        /// <summary>
        /// 移除指定的消息
        /// </summary>
        /// <param name="notificationName">消息名称</param>
        /// <param name="notifyContext">这个消息对应额Observer</param>
        public virtual void RemoveObserver(string notificationName, object notifyContext)
        {
            lock (m_syncRoot)
            {

                if (m_observerMap.ContainsKey(notificationName))
                {
                    IList<IObserver> observers = m_observerMap[notificationName];
                    for (int i = 0; i < observers.Count; i++)
                    {
                        if (observers[i].CompareNotifyContext(notifyContext))
                        {

                            observers.RemoveAt(i);
                            break;
                        }
                    }
                    if (observers.Count == 0)
                    {
                        m_observerMap.Remove(notificationName);
                    }
                }
            }
        }

        /// <summary>
        /// 注册对应的视图层
        /// </summary>
        /// <param name="mediator">视图层的名称和对应这个视图层的所属类</param>
        public virtual void RegisterMediator(IMediator mediator)
        {
            lock (m_syncRoot)
            {
                if (m_mediatorMap.ContainsKey(mediator.MediatorName)) return;

                m_mediatorMap[mediator.MediatorName] = mediator;

                IList<string> interests = mediator.ListNotificationInterests();//获取对应视图层所属的关注消息列表

                if (interests.Count > 0)
                {
                    IObserver observer = new Observer("handleNotification", mediator);//实例对应执行消息的Observer
                    for (int i = 0; i < interests.Count; i++)
                    {
                        RegisterObserver(interests[i].ToString(), observer);//消息名称和对应执行消息的Observer注册进m_observerMap（字典）
                    }
                }
            }
            mediator.OnRegister();
        }
        /// <summary>
        /// 检索对应的Meditor
        /// </summary>
        /// <param name="mediatorName"></param>
        /// <returns></returns>
        public virtual IMediator RetrieveMediator(string mediatorName)
        {
            lock (m_syncRoot)
            {
                if (!m_mediatorMap.ContainsKey(mediatorName)) return null;
                return m_mediatorMap[mediatorName];
            }
        }
        public virtual IMediator RemoveMediator(string mediatorName)
        {
            IMediator mediator = null;

            lock (m_syncRoot)
            {

                if (!m_mediatorMap.ContainsKey(mediatorName)) return null;
                mediator = (IMediator)m_mediatorMap[mediatorName];

                IList<string> interests = mediator.ListNotificationInterests();//获取要移除视图层关注的消息列表

                for (int i = 0; i < interests.Count; i++)
                {
                    RemoveObserver(interests[i], mediator);//移除对应的关注消息的Observer
                }
                m_mediatorMap.Remove(mediatorName);
            }
            if (mediator != null) mediator.OnRemove();
            return mediator;
        }
        /// <summary>
        /// 判断对应的视图层是否存在
        /// </summary>
        /// <param name="mediatorName">视图层名称</param>
        /// <returns></returns>
        public virtual bool HasMediator(string mediatorName)
        {
            lock (m_syncRoot)
            {
                return m_mediatorMap.ContainsKey(mediatorName);
            }
        }
    }
}
