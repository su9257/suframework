﻿using System;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;

namespace PureMVC.Core
{
    /// <summary>
    /// 控制核心
    /// </summary>
    public class Controller : IController
	{
        #region Single
        protected IView m_view;
        protected IDictionary<string, Type> m_commandMap;
        protected static volatile IController m_instance;
        protected readonly object m_syncRoot = new object();
        protected static readonly object m_staticSyncRoot = new object();
        static Controller()
        {
        }
        protected Controller()
		{
			m_commandMap = new Dictionary<string, Type>();	
			InitializeController();
		}
        protected virtual void InitializeController()
        {
            m_view = View.Instance;
        }
        public static IController Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (m_staticSyncRoot)
                    {
                        if (m_instance == null) m_instance = new Controller();
                    }
                }

                return m_instance;
            }
        }
        #endregion

        /// <summary>
        /// 执行命令 和执行消息类似，但是与消息的区别是每次执行命令消息是通过新实例化类执行
        /// </summary>
        /// <param name="note"></param>
        public virtual void ExecuteCommand(INotification note)
		{
			Type commandType = null;

			lock (m_syncRoot)
			{
				if (!m_commandMap.ContainsKey(note.Name)) return;
				commandType = m_commandMap[note.Name];
			}

			object commandInstance = Activator.CreateInstance(commandType);

			if (commandInstance is ICommand)
			{
				((ICommand) commandInstance).Execute(note);
			}
		}
        /// <summary>
        /// 注册命令消息
        /// </summary>
        /// <param name="notificationName">命令消息名称</param>
        /// <param name="commandType">对应继承ICommand的命令类</param>
		public virtual void RegisterCommand(string notificationName, Type commandType)
		{
			lock (m_syncRoot)
			{
				if (!m_commandMap.ContainsKey(notificationName))
				{

					m_view.RegisterObserver(notificationName, new Observer("executeCommand", this));
				}

				m_commandMap[notificationName] = commandType;
			}
		}


		public virtual bool HasCommand(string notificationName)
		{
			lock (m_syncRoot)
			{
				return m_commandMap.ContainsKey(notificationName);
			}
		}


		public virtual void RemoveCommand(string notificationName)
		{
			lock (m_syncRoot)
			{
				if (m_commandMap.ContainsKey(notificationName))
				{
					m_view.RemoveObserver(notificationName, this);
					m_commandMap.Remove(notificationName);
				}
			}
		}

	}
}
