﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Custom.Log;
#endregion


public static class GameObjectExtend
{
    /// <summary>
    /// 根据名称寻找指定子节点
    /// </summary>
    /// <param name="Self"></param>
    /// <param name="NodeName">节点名称</param>
    /// <returns></returns>
    public static GameObject FindChiledNode(this GameObject Self, string NodeName)
    {
        return FindChiledNode(Self, Self, NodeName);
    }
    /// <summary>
    /// 根据名称寻找指定子节点
    /// </summary>
    /// <param name="Self"></param>
    /// <param name="NodeName">需要找到节点的名称</param>
    /// <param name="Root">需要找到节点的根节点，默认值为调用此函数的GameObject</param>
    /// <returns></returns>
    public static GameObject FindChiledNode(this GameObject Self, GameObject Root, string NodeName)
    {
        if (string.IsNullOrEmpty(NodeName) || Root == null)
        {
            Debuger.LogWarning("参数为Null，请检查");
            return null;
        }
        List<Transform> TempNodeList = new List<Transform>();
        Root.GetComponentsInChildren(TempNodeList);

        foreach (var itemNode in TempNodeList)
        {
            Debug.Log("遍历：" + itemNode.name);
            if (itemNode.gameObject.name.Equals(NodeName))
            {
                Debug.Log("NodeName");
                return itemNode.gameObject;
            }
        }
        return null;
    }

    public static T FindNodeComponent<T>(this GameObject Self, string NodeName, GameObject Root) where T : Component
    {
        if (string.IsNullOrEmpty(NodeName) || Root == null)
        {

        }
        List<Transform> TempNodeList = new List<Transform>();
        //if (Root == null)
        //{
        //    Self.GetComponentsInChildren<Transform>(TempNodeList);
        //}
        //else
        //{
        //    Root.GetComponents<Transform>(TempNodeList);
        //}

        Root.GetComponents<Transform>(TempNodeList);
        foreach (var itemNode in TempNodeList)
        {
            if (itemNode.gameObject.name.Equals(NodeName))
            {
                return itemNode.GetComponent<T>();
            }
        }
        return default(T);
    }
}
