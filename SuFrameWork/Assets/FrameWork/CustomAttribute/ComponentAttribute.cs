﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Custom.Log;
#endregion

/*
 *     特性类的定位参数和命名参数的类型仅限于特性参数类型，这些包括：
 *     bool,byte,char,double,float,int,long,short,string,System.Type,object,enum类型，
 *     前提是它或任何有它嵌套在里面的类型必须是公共的可存取类型——就像在那个使用RegistryHives枚举的例子中一样。
 *     由上述的任何类型组成的一维数组。
 *     因为有效的参数类型仅局限于上述列出来的类型，因此您不能把一个像类那样的数据结构传递给特性构造函数作为参数。这种限制很有意义，因为特性是在程序设计时附加上的，
 *     此时您并没有这个类（对象）的实例化的实例。使用上面列出来的这些有效类型，您就可以在程序设计时把他们的值固定下来，就是为什么能使用他们的原因。
 */

public struct NodeInfo
{
    public string nodeName;
    public GameObject Node;
}

[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
public class FindChildNodeAttribute : Attribute
{
    public string nodeName;
    public FindChildNodeAttribute()
    {
        this.Log("UIAttribute构造函数");
    }
    public FindChildNodeAttribute(string nodeName)
    {
        this.nodeName = nodeName;
        this.Log("UIAttribute构造函数");
    }

    public GameObject FindChildNode(Dictionary<int, NodeInfo> rootDictionary)
    {
        NodeInfo SelfNodeInfo;
        SelfNodeInfo.Node = null;

        foreach (var value in rootDictionary.Values)
        {
            if (value.nodeName.Equals(nodeName))
            {
                SelfNodeInfo.Node = value.Node;
            }
        }
        return SelfNodeInfo.Node;
    }
}


