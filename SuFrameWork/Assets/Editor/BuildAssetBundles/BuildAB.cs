﻿#region using

using UnityEngine;
using UnityEditor;

#endregion

public class BuildAB : Editor
{
    [MenuItem("Tools/Build")]
    public static void BuildAssetBundle()
    {
        BuildPipeline.BuildAssetBundles(Application.streamingAssetsPath, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.Android);
    }
}
