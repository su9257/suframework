﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class SingleAnimationClip : Editor
{

    [MenuItem("Tools/AnimationClipCompressionCurve")]
    static void LoopFalse()
    {
        Object[] selectedAsset = Selection.GetFiltered(typeof(Object), SelectionMode.Unfiltered);
        Debug.Log("压缩动画精度" + "****");
        for (int j = 0 ; j < selectedAsset.Length ; j++)
        {
            Debug.Log("压缩动画精度" + selectedAsset + "****");
            AnimationClip clip = selectedAsset[0] as AnimationClip;
            //AnimationUtility.GetAnimationClipSettings(clip).loopTime = false;//不能直接赋值  
            //AnimationClipSettings clipSetting = AnimationUtility.GetAnimationClipSettings(clip);
            //clipSetting.loopTime = false;
            //AnimationUtility.SetAnimationClipSettings(clip, clipSetting);

            //浮点数精度压缩到f3
            AnimationClipCurveData[] curves = null;
            curves = AnimationUtility.GetAllCurves(clip);
            Keyframe key;
            Keyframe[] keyFrames;
            for (int ii = 0 ; ii < curves.Length ; ++ii)
            {
                Debug.Log("!!!!!");
                AnimationClipCurveData curveDate = curves[ii];
                if (curveDate.curve == null || curveDate.curve.keys == null)
                {
                    //Debug.LogWarning(string.Format("AnimationClipCurveData {0} don't have curve; Animation name {1} ", curveDate, animationPath));
                    Debug.Log("为null");
                    continue;
                }
                keyFrames = curveDate.curve.keys;
                for (int i = 0 ; i < keyFrames.Length ; i++)
                {
                    key = keyFrames[i];
                    key.value = float.Parse(key.value.ToString("#0.00"));
                    key.inTangent = float.Parse(key.inTangent.ToString("#0.00"));
                    key.outTangent = float.Parse(key.outTangent.ToString("#0.00"));
                    keyFrames[i] = key;
                }
                Debug.Log("修改curveDate.path:" + curveDate.path + "****" + curveDate.type + "***" + curveDate.propertyName + "***" + curveDate.curve);
                curveDate.curve.keys = keyFrames;
                clip.SetCurve(curveDate.path, curveDate.type, curveDate.propertyName, curveDate.curve);
            }
        }


    }

    [MenuItem("Tools/Log")]
    static void LogSelected()
    {
        Object[] selectedAsset = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
        for (int i = 0 ; i < selectedAsset.Length ; i++)
        {
            Debug.Log(selectedAsset[i]);
        }
    }
}
