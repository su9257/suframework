﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Custom.Log;
#endregion


public class MainMenuItemData
{
    public string ChemicalName = "";
    public Dictionary<string, string> ChemicalVuforiaDic = new Dictionary<string, string>();

    public MainMenuItemData(string TempChemicalName = "", Dictionary<string, string> TempChemicalVuforia = null)
    {
        ChemicalName = TempChemicalName;
        ChemicalVuforiaDic = TempChemicalVuforia;
    }

}
public class ExampleProxy : Proxy
{
    public new const string NAME = "ExampleProxy"; //本类名称

    public IList<MainMenuItemData> ItemDatas
    {
        get
        {
            return base.Data as IList<MainMenuItemData>;
        }
    }

    public ExampleProxy() : base(NAME, new List<MainMenuItemData>())
    {
        AddItemData(new MainMenuItemData("名称", new Dictionary<string, string> { { "名称", "名称1" } }));
        AddItemData(new MainMenuItemData(""));
        AddItemData(new MainMenuItemData(""));
        AddItemData(new MainMenuItemData(""));
        AddItemData(new MainMenuItemData(""));
        AddItemData(new MainMenuItemData(""));

    }
    public void AddItemData(MainMenuItemData TempItemData)
    {
        if (ItemDatas != null)
        {
            ItemDatas.Add(TempItemData);
        }
    }

    public override void OnRegister()
    {
        base.OnRegister();
        this.Log("OnRegister");
    }

}
