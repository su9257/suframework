﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Custom.Log;
#endregion

//视图层和组件分离
public class ExampleMediator:Mediator
{
    public new const string NAME = "ExampleMediator"; //本类名称

    public ExampleMediator(ExampleView ViewComponent) : base(NAME, ViewComponent)
    {

    }
    private ExampleView MainMenuViewComponent
    {
        get
        {
            return base.ViewComponent as ExampleView;
        }
    }


    public override IList<string> ListNotificationInterests()
    {
        IList<string> MsgList = new List<string>();
        MsgList.Add(NotificationMessage.Msg_InitMainMenu);
        MsgList.Add(NotificationMessage.Msg_OpenMainMenuPanel);
        MsgList.Add(NotificationMessage.Msg_CloseMainMenuPanel);
        MsgList.Add(NotificationMessage.Msg_LoadTeacherPanel);
        return MsgList;
    }

    /// <summary>
    /// 处理需要关注的消息
    /// </summary>
    /// <param name="notification"></param>
    public override void HandleNotification(INotification notification)
    {
        switch (notification.Name)
        {
            //初始化MainMenu界面
            case NotificationMessage.Msg_InitMainMenu:
                {

                }
                break;
            case NotificationMessage.Msg_OpenMainMenuPanel:
                {

                }
                break;
            case NotificationMessage.Msg_CloseMainMenuPanel:
                {

                }
                break;
            case NotificationMessage.Msg_LoadTeacherPanel:
                {

                }
                break;
            default:
                break;
        }
    }
}
