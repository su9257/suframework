﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;
using Custom.Log;
using System;
using UnityEngine.UI;
public class GameStart : MonoBehaviour,IDisposable
{
    #region Single
    public const string Name = "Singleton";
    static GameStart() { }
    protected GameStart() { }
    protected static volatile GameStart m_instance = null;
    protected readonly object m_syncRoot = new object();
    protected static readonly object m_staticSyncRoot = new object();

    public static GameStart Instance
    {
        get
        {
            return m_instance;
        }

        //set { m_instance = value; }  
    }
    #endregion
    public Font SelfFont = null;
    public GameObject Canvas_Low = null;
    public GameObject Canvas_Medium = null;
    public GameObject Canvas_High = null;

    public GameObject UICamera = null;
    public GameObject ARCamera = null;
    public GameObject ThreeDCamera = null;

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    void Awake ()
    {
        m_instance = this;

        //提前启动 不可运行任何 unity初始化的东西
        this.Log("GameStart");
        ApplicationFacade applicationFacade = ApplicationFacade.Instance as ApplicationFacade;
        DontDestroyOnLoad(this);
        //SceneManager.LoadSceneAsync("RunScene00");
    }
}
