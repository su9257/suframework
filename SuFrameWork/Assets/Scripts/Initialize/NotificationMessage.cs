﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Custom.Log;
#endregion

public class NotificationMessage
{
    /// <summary>
    /// 挂在vuforia需要的控制类
    /// </summary>
    public const string Commond_InitVuforia = "Commond_InitVuforia";
    /// <summary>
    /// 初始化MainMenu界面的，生成实验列表，注册点击事件
    /// </summary>
    public const string Msg_InitMainMenu = "InitMainMenu";

    /// <summary>
    /// 进入Vuforia场景
    /// </summary>
    public const string Msg_LoadVuforia = "Msg_LoadVuforia";
    /// <summary>
    /// 关闭MainMenuPanel界面
    /// </summary>
    public const string Msg_CloseMainMenuPanel = "Msg_CloseMainMenuPanel";
    /// <summary>
    /// 开启MainMenuPanel界面
    /// </summary>
    public const string Msg_OpenMainMenuPanel = "Msg_OpenMainMenuPanel";
    /// <summary>
    /// 载入Teacher讲解界面
    /// </summary>
    public const string Msg_LoadTeacherPanel = "Msg_OpenTeacherPanel";

    /// <summary>
    /// 离开Vuforia场景
    /// </summary>
    public const string Msg_LeaveVuforia = "LeaveVuforia";

    /// <summary>
    /// 触发VuforiaFound函数
    /// </summary>
    public const string Msg_VuforiaFound = "Msg_VuforiaFound";
    /// <summary>
    /// 触发VuforiaLost函数
    /// </summary>
    public const string Msg_VuforiaLost = "Msg_VuforiaLost";
    /// <summary>
    /// 固态掉落动画完成事件通知
    /// </summary>
    public const string Msg_SolidAnimationClipOver = "Msg_SolidAnimationClipOver";
    /// <summary>
    ///反应完成通知
    /// </summary>
    public const string Msg_LiquorAnimationClipOver = "Msg_LiquorAnimationClipOver";
    /// <summary>
    /// 点击溶液
    /// </summary>
    public const string Msg_TouchLiquor = "Msg_TouchLiquor";
    /// <summary>
    /// 微观演示动画完成
    /// </summary>
    public const string Msg_AnimationShow = "Msg_AnimationShow";

    /// <summary>
    /// 销毁播放滴管动画模型
    /// </summary>
    public const string Msg_DestoryDroper_ani = "Msg_DestoryDroper_ani";
}
