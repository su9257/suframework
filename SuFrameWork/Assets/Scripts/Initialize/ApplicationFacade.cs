﻿#region using
using System;
using System.Collections;
using System.Collections.Generic;
using PureMVC.Interfaces;
using PureMVC.Patterns;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Custom.Log;
#endregion

public class ApplicationFacade : Facade
{
    public new static IFacade Instance
    {
        get
        {
            if (m_instance == null)
            {
                lock (m_staticSyncRoot)
                {
                    if (m_instance == null)
                    {
                        m_instance = new ApplicationFacade();
                    }
                }
            }
            return m_instance;
        }

    }

    protected override void InitializeController()
    {
        base.InitializeController();
        RegisterCommand(NotificationMessage.Commond_InitVuforia,typeof( StartUpApplication));
    }

}
